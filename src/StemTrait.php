<?php

declare(strict_types=1);

namespace Drupal\views_porter_stemmer;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Wamania\Snowball\NotFoundException;
use Wamania\Snowball\StemmerManager;

/**
 * Trait with functionality common to all String filters.
 *
 * @internal
 */
trait StemTrait {

  /**
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * Options for this plugin will be held here.
   *
   * @var array
   */
  public $options = [];

  /**
   * The value of the filter.
   *
   * @var null|string
   */
  public $value = NULL;

  /**
   * The system logger, if available.
   *
   * @var null|\Psr\Log\LoggerInterface
   */
  protected ?\Psr\Log\LoggerInterface $logger = NULL;

  /**
   * Translates a string to the current language or to a given language.
   *
   * @param $string
   * @param array $args
   * @param array $options
   *
   * @see \Drupal\Core\StringTranslation\StringTranslationTrait::t()
   *
   * @return mixed
   */
  abstract protected function t($string, array $args = [], array $options = []);

  /**
   * Add a stemming option to the exposed views filter.
   *
   * @return array
   */
  public function defineOptions(): array {
    $options = parent::defineOptions();
    $options['expose']['contains']['stem_query'] = ['default' => FALSE];
    return $options;
  }

  /**
   * Default new filters to no stemming, to match core behaviour.
   */
  public function defaultExposeOptions(): void {
    $this->options['expose']['stem_query'] = FALSE;
  }

  /**
   * Add the stemming options to the exposed form.
   *
   * @param array &$form
   *   The form to add stemming options to.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function buildExposeForm(&$form, FormStateInterface $form_state): void {
    parent::buildExposeForm($form, $form_state);
    /** @noinspection HtmlUnknownTarget */
    $form['expose']['stem_query'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Stem the search query'),
      '#description' => $this->t('<a href="@link" target="_blank">Stemming</a> removes plurals and suffixes to match more results.', ['@link' => 'https://snowballstem.org/algorithms/porter/stemmer.html']),
      '#default_value' => $this->options['expose']['stem_query'],
    ];
  }

  /**
   * Stem the exposed filter value, if possible.
   */
  public function query(): void {
    if ($this->value && isset($this->options['expose']['stem_query']) && $this->options['expose']['stem_query']) {
      $manager = new StemmerManager();
      try {
        $new_values[] = $this->value;
        $new_values[] = $manager->stem($this->value, $this->languageManager->getCurrentLanguage()->getId());
        $this->value = implode(" ", $new_values);
      }
      catch (NotFoundException $e) {
        // The language does not support stemming. Since this can be an
        // expected case on multilingual sites, we do not log the exception even
        // as a notice to avoid spamming the logs.
      }
    }

    parent::query();
  }

}
