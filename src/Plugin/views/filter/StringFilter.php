<?php

declare(strict_types=1);

namespace Drupal\views_porter_stemmer\Plugin\views\filter;

use Drupal\Core\Database\Connection;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\views_porter_stemmer\StemTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Extends Drupal's String filter to support stemming.
 *
 * @ViewsFilter("views_porter_stemmer_string")
 */
class StringFilter extends \Drupal\views\Plugin\views\filter\StringFilter {

  use StemTrait;

  /**
   * Constructs a new StringFilter object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param LanguageManagerInterface $languageManager
   *   The language manager service used to determine the language to stem by.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Connection $connection, LanguageManagerInterface $languageManager) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $connection
    );
    $this->languageManager = $languageManager;
  }

  /**
   * Create a new StringFilter object.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('database'),
      $container->get('language_manager')
    );
  }

}
