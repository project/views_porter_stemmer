# Views Porter Stemmer

This module adds [stemming](https://snowballstem.org/algorithms/porter/stemmer.html) support to these Views filters:

- String
- Combine
- Views Autocomplete Filters extensions of both String and Combine
  - The Search API filters are not supported, as Search API already has equivalent functionality.

By default, stemming is off and can be turned in on the filter's exposed options.

Only some languages support stemming. For an up-to-date list of supported languages, see [the upstream library](https://github.com/wamania/php-stemmer/tree/master/src/Stemmer).
